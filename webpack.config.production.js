/**
 * Created by MC on 16/10/16.
 */
var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var HtmlWebpackPlugin = require('html-webpack-plugin');
var node_modules = path.resolve(__dirname, 'node_modules');
var pathToReact = path.resolve(node_modules, 'react/dist/react.min.js');
var pathToReactDom = path.resolve(node_modules, 'react-dom/dist/react-dom.min.js');
module.exports = {
    entry: [
        'webpack-hot-middleware/client?path=/__webpack_hmr&reload=true',            // 与 server 创建连接
        path.resolve(__dirname, './app/routes')
    ],
    output: {
        path: path.resolve(__dirname, './build'),
        filename: 'js/bundle.js',
        // publicPath:'http://localhost:8181/',         //同样可以
        publicPath:'/',
        chunkFilename: "js/[name].bundle.min.js?ver=[chunkhash]"
    },
    devServer:{
        // contentBase: '',  //静态资源的目录 相对路径,相对于当前路径 默认为当前config所在的目录
        // devtool: 'eval',
        // hot: true,        //自动刷新
        inline: true,    
        port: 8181        
    },
    module: {
        loaders: [
            {
                test: /\.(js|jsx)$/,
                exclude: /^node_modules$/,
                loader: 'babel',
            },{ 
                test: /\.(css|scss)$/, 
                // loader: ExtractTextPlugin.extract("style", "css!sass"),
                loader: ExtractTextPlugin.extract("style", "css!sass!postcss")
            },{
                test: /\.(png|jpg|jpeg|gif|eot|woff|svg|ttf|woff2|appcache)(\?|$)/,
                exclude: /^node_modules$/,
                loader: 'file-loader?name=img/[name].[ext]'
            }
        ]
    },
    resolve:{
        alias: {
          'react': pathToReact,
          'react-dom':pathToReactDom
        },
        extensions: ['', '.js', '.jsx'], //后缀名自动补全
    },
    /* 
    *   HtmlWebpackPlugin 这里会自动生成css和js文件,自己在build文件下创建的css文件和js文件目前未用到,但是先不删除,以防后期有变 
    *   有模板html文件,这样原先的 页面跳转了，title不能改变的问题
    */
    /*
    *   webpack-dev-middleware + webpack-hot-middleware仅适用于服务侧开发的场景
    *   webpack-dev-server 是在 webpack-dev-middleware 的基础上多套了一层壳来提供 CLI 及 server 能力
    */
    plugins: [
        new webpack.ProvidePlugin({
            React: 'react',
            ReactDOM: 'react-dom'
        }),
        new webpack.optimize.UglifyJsPlugin({minimize: true}),      //使用uglifyJs压缩js代码
        new webpack.optimize.OccurenceOrderPlugin(),                //按引用频度来排序 ID，以便达到减少文件大小的效果,webpack2已删除
        new webpack.HotModuleReplacementPlugin(),
        new webpack.optimize.CommonsChunkPlugin('js/common.js'),
        new webpack.DefinePlugin({   
            "process.env": { 
                NODE_ENV: JSON.stringify("production") 
            }
        }),
        // new ExtractTextPlugin("css/[name].css"),
        new ExtractTextPlugin("css/[name].[contenthash:6].css",{allChunks: true}),
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: `${__dirname}/build/index.html`,
            inject: 'body',
            hash: true
        })
    ]
};