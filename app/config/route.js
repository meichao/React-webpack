/*
*	created by MC on 2016/12/30
*/
// import _baseRoot from './const';
// 如果要将路径改成react这样,只需要将path设置成'/react/',其他页面只需要在其前面加'/react/即可',例如:/react/404
export default {
	childRoutes:[
		{
			path:'404',
			getComponent(nextState, cb) {
		    	require.ensure([], (require) => {
		      		cb(null, require('../public/tpl/NotFound'))
		    	})
		  	}
		},
		{
			path:'/',
			getComponent(nextState,cb){
				require.ensure([],(require)=>{
					cb(null,require('../public/tpl/App'))
				})
			},
			indexRoute:{
				getComponent(nextState,cb){
					require.ensure([],(require)=>{
						cb(null,require('../public/tpl/Main'))
					})
				},
			},
			childRoutes:[
				{
					path: 'component',
					// onEnter:(nextState, replace) => console.log("这里是子路由的过滤",nextState,replace),
				  	getComponent(nextState, cb) {
				    	require.ensure([], (require) => {
				      		cb(null, require('../public/tpl/ComponentMain'))
				    	})
				  	},
				  	indexRoute:{
						getComponent(nextState,cb){
							require.ensure([],(require)=>{
								cb(null,require('../public/tpl/componentInit'))
							})
						},
					},
					// onLeave:(nextState, replace) => console.log("这里是子路由的离开",nextState,replace),
					childRoutes:[
						{
							path:'table',
							getComponent(nextState,cb) {
								require.ensure([],(require) => {
									cb(null,require('../public/tpl/Table'))
								})
							}
						},
						{
							path:'picker',
							getComponent(nextState,cb) {
								require.ensure([],(require) => {
									cb(null,require('../public/tpl/Picker'))
								})
							}
						}
					]
			    },{
					path:'*',
					onEnter:(nextState, replace) => replace("404")
				}
			]
		}
	]	
}