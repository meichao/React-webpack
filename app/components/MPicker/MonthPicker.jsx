import React from 'react';
import classNames from 'classnames';
import {MInput,MIcon} from '../MComponent'
import {ComponentTool} from '../Tool/ComponentTool';
import moment from 'moment';
moment.locale();
/*
*	调用方式： <DatePicker onChange={this._changePicker} className={'demoClass'}/>
*
*	----------------------------------------------------------------------
*	|    参数     |	      说明	  	   |      类型	   |      默认值	 |
*	----------------------------------------------------------------------
*	|  onChange   |       回调函数 	   |    function   |				 |
*	----------------------------------------------------------------------
*	|  className  |    用户自定义样式    |     string    | 				 |
*	----------------------------------------------------------------------
*	|  initValue  |    用户初始化的时间    |     string    | 				 |
*	----------------------------------------------------------------------
*	|  placeholder  |    输入框中默认的提示    |     string    | 				 |
*	----------------------------------------------------------------------
*/
class MonthPicker extends React.Component{
	constructor(props) {
		super(props);
		this.state = {
			view:'MonthPicker',										//视图类别:MonthPicker,DecadePicker
			initValue:this.props.initValue,
			changeViewDate:this._initDate(),
			format:'YYYY-MM',
			monthOpen:false,
			placeholder:this.props.placeholder || '',
		};

		[
			'_initDate',
	        '_showPicker',
	        '_closePicker',
	        '_stopPropagation',
	        '_prevYear',
	        '_prevMonth',
	        '_nextMonth',
	        '_nextYear',
	        '_prevDecade',
	        '_nextDecade',
	        '_prevCentury',
	        '_nextCentury',
	        '_clearMonth',
	        '_selectMonth',
	        '_selectYear',
	        '_selectDecade',
	        '_changeView',
	        '_renderMonthPicker',
	        '_renderYearPicker',
	        '_renderDecadePicker',
      	].forEach(func=>{
          	this[func] = this[func].bind(this);
      	});
	}
	_initDate(date){
		return moment(date || this.props.initValue || new Date()).format('YYYY-MM');
	}
	_showPicker(e){
		this._stopPropagation(e);
	    this.setState({
      		monthOpen:true
	    }) 
	}
	_closePicker(){
		let newState = this.state;
	    if(newState.monthOpen){
	    	newState.monthOpen = false;
	    	newState.view='MonthPicker';
	    	newState.changeViewDate = !!newState.initValue && moment(newState.initValue).isValid()?newState.initValue:newState.changeViewDate;
			this.setState(newState)
	    } 
	}
	_stopPropagation(e){
		e.stopPropagation();
		if (e.nativeEvent.stopImmediatePropagation) {
      		e.nativeEvent.stopImmediatePropagation();
	    }
	}
	_prevYear(e){
		let newState = this.state;
		newState.changeViewDate = this._initDate(moment(newState.changeViewDate).subtract(1,'years'));
		this.setState(newState);
	}
	_prevMonth(e){
		let newState = this.state;
		newState.changeViewDate = this._initDate(moment(newState.changeViewDate).subtract(1,'months'));
		this.setState(newState);
	}
	_nextMonth(e){
		let newState = this.state;
		newState.changeViewDate = this._initDate(moment(newState.changeViewDate).add(1,'months'));
		this.setState(newState);
	}
	_nextYear(e){
		let newState = this.state;
		newState.changeViewDate = this._initDate(moment(newState.changeViewDate).add(1,'years'));
		this.setState(newState);
	}
	_prevDecade(e){
		let newState = this.state;
		newState.changeViewDate = this._initDate(moment(newState.changeViewDate).subtract(10,'years'));
		this.setState(newState);
	}
	_nextDecade(e){
		let newState = this.state;
		newState.changeViewDate = this._initDate(moment(newState.changeViewDate).add(10,'years'));
		this.setState(newState);
	}
	_prevCentury(e){
		let newState = this.state;
		newState.changeViewDate = this._initDate(moment(newState.changeViewDate).subtract(100,'years'));
		this.setState(newState);
	}
	_nextCentury(e){
		let newState = this.state;
		newState.changeViewDate = this._initDate(moment(newState.changeViewDate).add(100,'years'));
		this.setState(newState);
	}
	_clearMonth(e){
		this._stopPropagation(e);
		let newState = this.state;
		newState.initValue = '';
		newState.changeViewDate = this._initDate(new Date());
		this.setState(newState);
	}
	_selectMonth(formDate){
		let newState = this.state;
		newState.initValue = formDate;
		newState.changeViewDate = formDate;
		newState.monthOpen = false;
		this.setState(newState);
		this.props.onChange?this.props.onChange(moment(formDate),formDate):"";
	}
	_selectYear(formDate){
		let newState = this.state;
		newState.changeViewDate = formDate;
		newState.view = 'MonthPicker';
		this.setState(newState);
	}
	_selectDecade(formDate){
		let newState = this.state;
		newState.changeViewDate = formDate;
		newState.view = 'YearPicker';
		this.setState(newState);
	}
	_changeView(view){
		let newState = this.state;
		newState.view = view;
		this.setState(newState);
	}
	_renderMonthPicker(){
		let that = this;
		let changeViewDate = that.state.changeViewDate;
		let format = that.state.format;
		if(moment(changeViewDate).isValid()){
			const MonthArr = moment.monthsShort(); // 获取月份的缩写
			let MonthRenderArr = [];
			let MonthIndex = 0;
			// 将一维数组变成二维数组然后进行循环
			while(MonthIndex < MonthArr.length){
				MonthRenderArr.push(MonthArr.slice(MonthIndex,MonthIndex+3));
				MonthIndex += 3;
			}
			return (
		    	<div className={classNames('M-calendar-MonthPicker')}>
		    		<div className={classNames('M-calendar-month-panel-header M-calendar-header')}>
		    			<a title='Last Year' className={classNames('M-calendar-month-panel-prev-year-btn')} onClick={that._prevYear}></a>
		    			<a title='Choose a year' className={classNames('M-calendar-month-panel-year-select')} onClick={that._changeView.bind(that,'YearPicker')}>
		    				<span>{moment(changeViewDate).year()}</span>
		    			</a>
		    			<a title='Next Year' className={classNames('M-calendar-month-panel-next-year-btn')} onClick={that._nextYear}></a>
		    		</div>
		    		<div className={classNames('M-calendar-month-panel-body M-calendar-tabel-parent')}>
		    			<table className={classNames('M-calendar-month-panel-table')}>
		    				<tbody>
		    					{
		    						MonthRenderArr.map(function(elem,elemIndex) {
			    						return (
			    							<tr key={`M-calendar-month-column-body-tr-${elemIndex}`}>
			    							{
			    								elem.map(function(childElem,childElemIndex) {
		    										return (
		    											<td title={childElem} className={classNames('M-calendar-month-panel-cell',{'M-calendar-month-panel-selected-cell':childElem==MonthArr[moment(changeViewDate).get('month')]})} key={`M-calendar-month-column-body-td-${elemIndex}-${childElemIndex}`} onClick={that._selectMonth.bind(that,moment(changeViewDate).month(childElem).format(format))}>
		    												<a className={classNames('M-calendar-month-panel-month')}>{childElem}</a>
		    											</td>
		    										)
		    									})
	    									}
	    									</tr>
			    						)
			    					})
		    					}
		    				</tbody>
		    			</table>
		    		</div>
		    	</div>
		    );
		}else{
			// 这里其实应该处理的更加友好一点
			console.log('这里是错误的日期格式');
			return false;
		}
	}
	_renderYearPicker(){
		let that = this;
		let changeViewDate = that.state.changeViewDate;
		let format = that.state.format;
		if(moment(changeViewDate).isValid()){
			const CurrentYear = moment(changeViewDate).year(); // 获取当前的年份
			let CurrentDecade = parseInt(CurrentYear/10)*10;
			let CurrentDecadeLast = CurrentDecade + 9;
			let YearArr = [];
			let YearRenderArr = [];
			for(let i=0;i<=11;i++){
				YearArr.push(CurrentDecade+i-1);
			}
			let YearIndex = 0;
			// 将一维数组变成二维数组然后进行循环
			while(YearIndex < YearArr.length){
				YearRenderArr.push(YearArr.slice(YearIndex,YearIndex+3));
				YearIndex += 3;
			}
			return (
		    	<div className={classNames('M-calendar-YearPicker')}>
		    		<div className={classNames('M-calendar-year-panel-header M-calendar-header')}>
		    			<a title='Last Decade' className={classNames('M-calendar-year-panel-prev-decade-btn')} onClick={that._prevDecade}></a>
		    			<a title='Choose a decade' className={classNames('M-calendar-year-panel-decade-select')} onClick={that._changeView.bind(that,'DecadePicker')}>
		    				<span>{CurrentDecade}-{CurrentDecadeLast}</span>
		    			</a>
		    			<a title='Next Decade' className={classNames('M-calendar-year-panel-next-decade-btn')} onClick={that._nextDecade}></a>
		    		</div>
		    		<div className={classNames('M-calendar-year-panel-body M-calendar-tabel-parent')}>
		    			<table className={classNames('M-calendar-year-panel-table')}>
		    				<tbody>
		    					{
		    						YearRenderArr.map(function(elem,elemIndex) {
			    						return (
			    							<tr key={`M-calendar-year-column-body-tr-${elemIndex}`}>
			    							{
			    								elem.map(function(childElem,childElemIndex) {
		    										return (
		    											<td title={childElem} className={classNames('M-calendar-year-panel-cell',{'M-calendar-year-panel-selected-cell':childElem==moment(changeViewDate).year()},{'M-calendar-year-panel-last-decade-cell':childElem<CurrentDecade},{'M-calendar-year-panel-next-decade-cell':childElem>CurrentDecadeLast})} key={`M-calendar-month-column-body-td-${elemIndex}-${childElemIndex}`} onClick={that._selectYear.bind(that,moment(changeViewDate).year(childElem).format(format))}>
		    												<a className={classNames('M-calendar-year-panel-year')}>{childElem}</a>
		    											</td>
		    										)
		    									})
	    									}
	    									</tr>
			    						)
			    					})
		    					}
		    				</tbody>
		    			</table>
		    		</div>
		    	</div>
		    );
		}else{
			// 这里其实应该处理的更加友好一点
			console.log('这里是错误的日期格式');
			return false;
		}
	}
	_renderDecadePicker(){
		let that = this;
		let changeViewDate = that.state.changeViewDate;
		let format = that.state.format;
		if(moment(changeViewDate).isValid()){
			const CurrentYear = moment(changeViewDate).year(); // 获取当前的年份
			let CurrentCentury = parseInt(CurrentYear/100); // 获取世纪
			let CurrentCenturyDecade = CurrentCentury*100;	   // 获取当前世纪的首个年代
			let CenturyArr = [];
			let CenturyRenderArr = [];
			for(let i=0;i<=11;i++){
				let currentYearFirst = parseInt(CurrentCenturyDecade+(i-1)*10);
				let currentDecade = parseInt((CurrentCenturyDecade+(i-1)*10)/10);
				let currentCentery = parseInt(currentDecade/10);
				let decadeObj = {
					currentYearFirst,
					currentDecade,
					currentCentery,
					currentDecadeRange:`${currentYearFirst}-${currentYearFirst+9}`,
					classType:currentCentery > CurrentCentury?
								'M-calendar-decade-panel-next-century-cell':currentCentery < CurrentCentury?
									'M-calendar-decade-panel-last-century-cell':currentYearFirst<=CurrentYear&&CurrentYear<=(currentYearFirst+9)?
										'M-calendar-decade-panel-selected-cell':''
				}
				CenturyArr.push(decadeObj);//算出年代
			}
			let CenturyIndex = 0;
			// 将一维数组变成二维数组然后进行循环
			while(CenturyIndex < CenturyArr.length){
				CenturyRenderArr.push(CenturyArr.slice(CenturyIndex,CenturyIndex+3));
				CenturyIndex += 3;
			}
			return (
		    	<div className={classNames('M-calendar-DecadePicker')}>
		    		<div className={classNames('M-calendar-decade-panel-header M-calendar-header')}>
		    			<a title='Last Century' className={classNames('M-calendar-decade-panel-prev-century-btn')} onClick={that._prevCentury}></a>
		    			<a className={classNames('M-calendar-decade-panel-century-select')}>
		    				<span>{CurrentCenturyDecade}-{CurrentCenturyDecade+99}</span>
		    			</a>
		    			<a title='Next Century' className={classNames('M-calendar-decade-panel-next-century-btn')} onClick={that._nextCentury}></a>
		    		</div>
		    		<div className={classNames('M-calendar-decade-panel-body M-calendar-tabel-parent')}>
		    			<table className={classNames('M-calendar-decade-panel-table')}>
		    				<tbody>
		    					{
		    						CenturyRenderArr.map(function(elem,elemIndex) {
			    						return (
			    							<tr key={`M-calendar-decade-column-body-tr-${elemIndex}`}>
			    							{
			    								elem.map(function(childElem,childElemIndex) {
		    										return (
		    											<td className={classNames('M-calendar-decade-panel-cell',childElem.classType)} key={`M-calendar-decade-column-body-td-${elemIndex}-${childElemIndex}`} onClick={that._selectDecade.bind(that,moment(changeViewDate).year(childElem.currentYearFirst).format(format))}>
		    												<a className={classNames('M-calendar-decade-panel-decade')}>{childElem.currentDecadeRange}</a>
		    											</td>
		    										)
		    									})
	    									}
	    									</tr>
			    						)
			    					})
		    					}
		    				</tbody>
		    			</table>
		    		</div>
		    	</div>
		    );
		}else{
			// 这里其实应该处理的更加友好一点
			console.log('这里是错误的日期格式');
			return false;
		}
	}
	componentDidMount(){
    	ComponentTool.bind(document, 'click', this._closePicker);
  	}
  	componentWillUnmount(){
    	ComponentTool.unbind(document, 'click', this._closePicker);
  	}
	render(){
		let that = this;
		let initValue = !!that.state.initValue && moment(that.state.initValue).isValid()?that._initDate(that.state.initValue):that.state.initValue;
		let pickerContainerStay = this.props.pickerContainerStay? `M-calendar-picker-container-placement-${this.props.pickerContainerStay}`:'M-calendar-picker-container-placement-bottomLeft';
		return (
			<div className={classNames('M-calendar-picker',that.props.className,{'M-calendar-picker-open':that.state.monthOpen})}>
				<div onClick={that._showPicker} className={classNames('M-calendar-picker-input-area')}>
					<MInput value={initValue} placeholder={that.props.placeholder} readOnly/>
					<span className={classNames('M-calendar-picker-icon')} onClick={that._showPicker}><MIcon type='calendar' /></span>
					{!!that.state.initValue && moment(that.state.initValue).isValid()?<span className={classNames('M-calendar-picker-clear')} onClick={that._clearMonth}><MIcon type='closecircle'/></span>:''}
				</div>
				{
					that.state.monthOpen?(
						<div onClick={that._stopPropagation} className={classNames('M-calendar-picker-container',pickerContainerStay)}>
							{
								that.state.view == 'MonthPicker'?
									that._renderMonthPicker():that.state.view == 'YearPicker'?
										that._renderYearPicker():that.state.view == 'DecadePicker'?
											that._renderDecadePicker():''
							}
						</div>
					):''
				}
			</div>
		)
	}
}
module.exports = MonthPicker;