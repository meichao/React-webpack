import MButton from './MButton/index';
import MInput from './MInput/index';
import MTooltip from './MTooltip/index';
import MBackTop from './MBackTop/index';
import MIcon from './MIcon/index';
import MPagination from './MPagination/index';
import MSelect from './MSelect/index';
import MTable from './MTable/index';
import MPicker from './MPicker/index';
export {MButton,MTooltip,MInput,MBackTop,MIcon,MPagination,MSelect,MTable,MPicker}

