import React from 'react';
import classNames from 'classnames';
import MInput from './MInput';
import {ComponentTool} from '../Tool/ComponentTool';
import moment from 'moment';
moment.locale();
/*
*	调用方式： <MBackTop onClick={this._callback} className={'demoClass'}/>
*
*	----------------------------------------------------------------------
*	|  参数		|	      说明	  	   |      类型	   |      默认值	 |
*	----------------------------------------------------------------------
*	|  onClick  |       回调函数 	   |    function   |				 |
*	----------------------------------------------------------------------
*	| className |    用户自定义样式    |     string    | 				 |
*	----------------------------------------------------------------------
*/
export default class DatePicker extends React.Component{
	constructor(props) {
		super(props);
		this.state = {
			defaultValue:this.props.defaultValue || {},
			format:this.props.format || 'YYYY-MM-DD hh:mm:ss',
			dateOpen:false,
			placeholder:this.props.placeholder || '',
		};

		[
	        '_dateShow',
	        '_dateClose',
	        '_changeDate',
	        '_stopPropagation',
      	].forEach(func=>{
          	this[func] = this[func].bind(this);
      	});
	}
	_dateShow(e){
	    let ifOpen = this.state.dateOpen;
	    this.setState({
      		dateOpen:!ifOpen
	    }) 
		// 应该要把时间存储下来的
		console.log('这里需要显示日期插件了');
		console.log(e.target.value);
	}
	_dateClose(){
		let ifOpen = this.state.dateOpen;
	    if(ifOpen){
	      this.setState({
	        dateOpen:!ifOpen
	      })
	    } 
	}
	_changeDate(e){
		// 需要对输入的内容进行过滤，如果满足时间条件,则刷新state,更新时间
		console.log('这里是输入框中内容改变');
		if(moment(e.target.value,this.state.format).isValid()){
			console.log(e.target.value);
		}
	}
	_stopPropagation(e){
		if (e.nativeEvent.stopImmediatePropagation) {
      		e.nativeEvent.stopImmediatePropagation();
	    }
	}
	componentDidMount(){
    	ComponentTool.bind(document, 'click', this._dateClose);
  	}
  	componentWillUnmount(){
    	ComponentTool.unbind(document, 'click', this._dateClose);
  	}
	render(){
		console.log(this.state)
		const daysArr = [[], [], [], [], [], []];
		let defaultValue = moment(this.state.defaultValue).format(this.state.format);
		// console.log(moment(this.state.defaultValue).date(1).weekday())
	 	const currentWeekday = moment(this.state.defaultValue).date(1).weekday(); // 获取当月1日为星期几
	    const lastMonthDays = moment(this.state.defaultValue).subtract(1, 'month').daysInMonth(); // 获取上月天数
	    const currentMonthDays = moment(this.state.defaultValue).daysInMonth(); // 获取当月天数
	    const getDay = day => (day <= lastMonthDays ? day : (day <= (lastMonthDays + currentMonthDays)) ? day - lastMonthDays : day - (lastMonthDays + currentMonthDays)); // 日期处理
	    for (let i = 0; i < 7; i += 1) {
	      let virtualDay = (lastMonthDays - currentWeekday) + i;
	      for (let j = 0; j < 6; j += 1) {
	        daysArr[j][i] = getDay(virtualDay + (j * 7));
	      }
	    }
    	console.table(daysArr);
		let pickerContainerStay = this.props.pickerContainerStay? `M-picker-container-placement-${this.props.pickerContainerStay}`:'M-picker-container-placement-bottomLeft';
		return (
			<div className={classNames('M-picker',this.props.className,{'M-picker-open':this.state.dateOpen})}>
				<div onClick={this._stopPropagation}>
					<MInput onFocus={this._dateShow} onChange={this._changeDate} defaultValue={defaultValue} placeholder={this.props.placeholder}/>
				</div>
				<div className={classNames('M-picker-container',pickerContainerStay)}>
					haha
				</div>
			</div>
		)
	}
}
class MonthPicker extends React.Component{
	render(){}
}
class RangePicker extends React.Component{
	render(){}
}
export {MonthPicker,RangePicker}

/*
*	datePicker插件问题点:
*		1:需要根据传入的时间(其实在想是不是根据传入的format的格式会比较好)来进行年、月、日、时、分(秒有待考虑)组件的判断,而且默认是显示时间格式最后的时间对应的组件
*		2:输入框中的时间改变的时候需要进行格式判断并实时更替组件
*		3:日期格式的错误预防已经提示
*		4:目前这样写的方式是有问题的,很多东西都是可以重复用到的,考虑下是否可以整合起来
*
*/