import React from 'react';
import { MPagination,MButton,MInput,MIcon } from '../../components/MComponent';
// import { Icon } from 'antd';

class ComponentInit extends React.Component{
	constructor(props){
		super(props);
		[
	      '_onShowSizeChange',
	      '_changePage',
	      '_callback',   
	    ].forEach(func=>{
	        this[func] = this[func].bind(this);
	    });
	};
	_onShowSizeChange(current,pageSize){
		// console.log("current",current);
		// console.log("pageSize",pageSize);
	}
	_changePage(current,pageSize){
		// console.log("current",current);
		// console.log("pageSize",pageSize);
	}
	_callback(){
		this.refInput._Focus();
	}
	render(){
		return (
			<article className={'McomponentInit'} style={{minHeight:900}}>
				<h1>React Component</h1>
				<section>
					<MButton onClick={this._callback} className={'demoClass'} type='primary' size='lg'>testword</MButton>
					<MInput type='text' size='lg' ref={event=>this.refInput=event}/>
				</section>
				<MPagination total="91" showSizeChanger showQuickJumper onShowSizeChange={this._onShowSizeChange} onChange={this._changePage}/>
			</article>
		)
	}
}

module.exports = ComponentInit;