import React from 'react';
import {MTable} from '../../components/MComponent';
// import { Icon } from 'antd';

class Table extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			name:'梅超',
			columns : [
			{
			  title: 'Name',
			  dataIndex: 'name',
			  key: 'name',
			  render: text => <a href="#">{text}{
			  	// console.log('textasdsad',text)
			  }</a>,
			}, {
			  title: 'Age',
			  dataIndex: 'age',
			  key: 'age',
			}, {
			  title: 'Address',
			  dataIndex: 'address',
			  key: 'address',
			}, {
			  title: 'Action',
			  key: 'action',
			  render: (text, record,index) => (
			    <span>
			    {
			    	// console.log('record',record)
				}
			      <a href="#">Action 一 {record.name}</a>
			      <span className="ant-divider" />
			      <a href="#">Delete</a>
			      <span className="ant-divider" />
			      <a href="#" className="ant-dropdown-link">
			        More actions
			      </a>
			    </span>
			  ),
			}],
			pagination : {
		    	current:1,
		    	pageSize:10,
	    	}
		};
	};
	render(){
		const data = [];
		for (let i = 0; i < 106; i++) {
		  data.push({
		    key: i,
		    name: `Edward King ${i}`,
		    age: i,
		    address: `London, Park Lane no. ${i}`,
		  });
		}
		return (
			<article className={'McomponentInit'} style={{minHeight:900}}>
				<MTable onChange={this._changePage} rowKey={record => record.registered} dataSource={data} columns={this.state.columns} pagination={this.state.pagination}/>
			</article>
		)
	}
}

module.exports = Table;