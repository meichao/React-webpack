/**
 * Created by MC on 2016/11/11.
 */
import React from 'react';
import {IndexLink,Link} from 'react-router';
import _baseRoot from '../../config/const';
class Header extends React.Component{
  render(){
    let url = {
      base:`${_baseRoot}`,
      component:`${_baseRoot}component`,
      contact:`${_baseRoot}contact`
    }
    return (
        <header className="header clearfix">
          <ul className="M-menu M-menu-horizontal">
            <li className="M-menu-item" key="home"><IndexLink to={url.base} activeClassName="headerActive">首页</IndexLink></li>
            <li className="M-menu-item" key="component"><Link to={url.component} activeClassName="headerActive">组件</Link></li>
            <li className="M-menu-item" key="contact"><Link to={url.contact} activeClassName="headerActive">下载</Link></li>
          </ul>
        </header>      
    )
  }
}
module.exports = Header;