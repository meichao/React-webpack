import React from 'react';
import {MPicker,MInput} from '../../components/MComponent';
// import { Icon } from 'antd';
const { MonthPicker} = MPicker;

class Picker extends React.Component{
	constructor(props){
		super(props);
		[
	        '_changePicker',
      	].forEach(func=>{
          	this[func] = this[func].bind(this);
      	});
	};
	_changePicker(date,dateString){
		console.log(date,dateString);
	}
	render(){
		return (
			<article className={'McomponentInit'} style={{minHeight:900}}>
				<div>
					<MPicker initValue='' placeholder='meichao' onChange={this._changePicker}/>
				</div>
				<div>
					<MonthPicker initValue='' placeholder='meichao' onChange={this._changePicker}/>
				</div>
			</article>
		)
	}
}

module.exports = Picker;